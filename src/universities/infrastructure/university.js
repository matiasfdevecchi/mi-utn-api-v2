var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("Conected University!");
});

var university_schema = new Schema({
    value: {
        type: String,
        required: [true, "Valor es requerido."],
    },
    created_at: {
        type: Date,
    },
    updated_at: {
        type: Date,
    },
});

university_schema.pre("save", function (next) {
    const now = new Date;
    this.updated_at = now;
    if (this.isNew) {
        this.created_at = now;
    }
    next();
});

university_schema.statics.create = async (value) => {
    try {
        const university = new University({
            value
        })
        await university.save();
        return {
            status: 201,
            json: {
                msg: 'Universidad creada con éxito.',
                id: university._id,
            },
        }
    } catch (e) {
        return {
            status: 500,
            json: {
                msg: 'No se ha podido crear la universidad. Inténtelo nuevamente.',
            },
        }
    }
}

university_schema.statics.find = async id => {
    return await University.findById(id);
}

university_schema.statics.getAll = async () => {
    try {
        const universities = await University.find();
        return {
            status: 200,
            json: {
                universities
            }
        }
    } catch (e) {
        return {
            status: 500,
            json: {
                msg: 'No se pudo obtener las universidades.'
            }
        }
    }
}

university_schema.statics.updateById = async (id, value) => {
    try {
        const university = await University.findById(id);
        if (!university) {
            return {
                status: 404,
                json: {
                    msg: 'Universidad no encontrada.',
                },
            };
        }
        university.value = value;
        await university.save();
        return {
            status: 200,
            json: {
                msg: 'Universidad actualizada.',
            }
        }
    } catch (e) {
        return {
            status: 500,
            json: {
                msg: 'No se ha podido actualizar la universidad. Inténtelo nuevamente.',
            }
        }
    }
}

university_schema.statics.deleteById = async (id) => {
    try {
        const university = await University.findByIdAndDelete(id);
        if (!university) {
            return {
                status: 404,
                json: {
                    msg: 'Universidad no encontrada.',
                },
            };
        }
        return {
            status: 200,
            json: {
                msg: 'Universidad eliminada.',
            }
        }
    } catch (e) {
        return {
            status: 500,
            json: {
                msg: 'No se ha podido eliminar la universidad. Inténtelo nuevamente.',
            }
        }
    }
}

var University = mongoose.model("University", university_schema);

module.exports = University;