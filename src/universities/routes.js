const express = require('express');
const validateInput = require('../middlewares/validateInput');
const validateIsAdminRole = require('../middlewares/validateIsAdminRole');
const validateToken = require('../middlewares/validateToken');
const CreateUniversity = require('./actions/create');
const { CreateValidator } = require('./validators');
const router = express.Router()

router.post('/', [validateToken, validateIsAdminRole, CreateValidator, validateInput], async (req, res) => {
    const { status, json } = await CreateUniversity(req.body.value);
    return res.status(status).json(json);
})

module.exports = router;