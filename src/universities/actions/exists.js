const University = require("../infrastructure/university")

module.exports = async id => {
    const u = await University.find(id);
    return u != null;
}