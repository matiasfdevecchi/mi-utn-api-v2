const University = require("../infrastructure/university")

module.exports = async value => {
    return await University.create(value);
}