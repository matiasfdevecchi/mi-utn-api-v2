var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("Conected User!");
});

var email_match = [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, "Email invalido"];

var user_schema = new Schema({
    email: {
        type: String,
        required: [true, "Email es requerido."],
        match: email_match
    },
    password: {
        type: String, 
        required: [true, "Contraseña es requerida."],
    },
    role: {
        type: Schema.Types.ObjectId,
        ref: 'Role',
        required: true,
    },
    last_connection: {
        type: Date,
    },
    created_at: {
        type: Date,
    },
    updated_at: {
        type: Date,
    },
});

user_schema.pre("save", function (next) {
    const now = new Date;
    this.updated_at = now;
    if (this.email)
        this.email = this.email.toLowerCase();
    if (this.isNew) {
        this.created_at = now;
    }
    next();
});

user_schema.statics.find = async id => {
    return await User.findById(id);
}

user_schema.statics.findByEmail = async (email) => {
    return await User.findOne({email: email.toLowerCase()});
}

user_schema.statics.create = async (email, password, role) => {
    try {
        const user = new User({
            email,
            password,
            role,
            last_connection: new Date(),
        })
        await user.save();
        const json = {
            ok: true,
            msg: 'Usuario creado con éxito.',
        }
        return {
            status: 201,
            json,
            uid: user._id,
        }
    } catch (e) {
        const json = {
            ok: false,
            msg: 'No se ha podido crear el usuario. Inténtelo nuevamente.',
        }
        return {
            status: 500,
            json,
        }
    }
} 

var User = mongoose.model("User", user_schema);

module.exports = User;