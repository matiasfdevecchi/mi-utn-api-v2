const User = require("../infrastructure/user")
const bcrypt = require('bcryptjs');
const generateToken = require('./token_service')

module.exports.isEmailInUse = async (email) => {
    const user = await User.findByEmail(email);
    return user !== null;
}

module.exports.loginWithEmail = async (email, password) => {
    try {
        const user = await User.findByEmail(email);
        if (!user) {
            const json = {
                ok: false,
                msg: 'No existe usuario con ese email.'
            };
            return {
                status: 400,
                json,
            }
        }

        const validPassword = bcrypt.compareSync(password, user.password);
        if (!validPassword) {
            const json = {
                ok: false,
                msg: 'Contraseña incorrecta.'
            };
            return {
                status: 400,
                json,
            }
        }

        const token = await generateToken(user.id);

        const json = {
            ok: true,
            uid: user.id,
            token,
            msg: 'Logueado con éxito.'
        }
        return {
            status: 200,
            json,
        }
    } catch (error) {
        const json = {
            ok: false,
            msg: 'Vuelve a intentarlo.'
        }
        return {
            status: 500,
            json,
        }
    }
}

module.exports.registerWithEmail = async (email, password, role) => {
    const salt = bcrypt.genSaltSync();
    const hashedPassword = bcrypt.hashSync(password, salt);
    const {status, json, uid} = await User.create(email, hashedPassword, role);
    const token = await generateToken(uid);
    return {
        status,
        json: {
            ...json,
            uid,
            token,
        }
    };
}

module.exports.refreshToken = async (req) => {
    const { uid} = req;
    const token = await generateToken(uid);
    return {
        status: 200,
        json: {
            msg: 'Token refrescado.',
            uid,
            token,
        }
    };
}