const jwt = require('jsonwebtoken');

module.exports = (uid) => {

    return new Promise((resolve, reject) => {

        const payload = {
            uid,
            created_at: new Date(),
        };

        jwt.sign(payload, process.env.SECRET_JWT_SEED, (err, token) => {
            if (err) {
                console.log(err);
                reject('No se pudo generar el token.');
            }
            resolve(token);
        })
    })
}