const { check } = require("express-validator");
const IsEmailInUse = require("../actions/is_email_in_use");

module.exports.LoginWithEmailValidator = [
    check('email')
        .not()
        .isEmpty()
        .withMessage('El email es requerido')
        .isLength({ min: 5, max: 50 })
        .isEmail()
        .withMessage('No es un email valido'),
    check('password')
        .not()
        .isEmpty()
        .withMessage('La contraseña es requerida')
        .isLength({ min: 6, max: 30 })
        .withMessage('Contraseña debe ser de entre 6 y 30 caracteres.'),
]

module.exports.RegisterWithEmailValidator = [
    check('email')
        .not()
        .isEmpty()
        .withMessage('El email es requerido')
        .isLength({ min: 5, max: 50 })
        .isEmail()
        .withMessage('No es un email valido')
        .custom(async (value) => {
            const isInUse = await IsEmailInUse(value);
            if (isInUse)
                throw new Error('Email en uso.')
        }),
    check('password')
        .not()
        .isEmpty()
        .withMessage('La contraseña es requerida')
        .isLength({ min: 6, max: 30 })
        .withMessage('Contraseña debe ser de entre 6 y 30 caracteres.'),
]