const express = require('express');
const validateInput = require('../../middlewares/validateInput');
const validateToken = require('../../middlewares/validateToken');
const GetDefaultRole = require('../../roles/actions/get_default_role');
const router = express.Router()
const { LoginWithEmail } = require('../actions/login_with_email');
const { RefreshToken } = require('../actions/refresh_token');
const { RegisterWithEmail } = require('../actions/register_with_email');
const { LoginWithEmailValidator, RegisterWithEmailValidator } = require('./users_validators');

router.post('/', [RegisterWithEmailValidator, validateInput], async (req, res) => {
    const {status:roleStatus, json:roleJson} = await GetDefaultRole();
    const { ok } = roleJson;
    if (!ok) {
        return res.status(roleStatus).json(roleJson);
    }
    
    const {status, json} = await RegisterWithEmail(req, roleJson.id);
    return res.status(status).json(json);
})

router.post('/login', [LoginWithEmailValidator, validateInput], async (req, res) => {
    const {status, json} = await LoginWithEmail(req);
    return res.status(status).json(json);
})

router.get('/renew', validateToken, async (req, res) => {
    const {status, json} = await RefreshToken(req);
    return res.status(status).json(json);
})

module.exports = router;