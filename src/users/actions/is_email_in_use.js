const { isEmailInUse } = require("../services/user_service")

module.exports = async (email) => {
    return await isEmailInUse(email);
}