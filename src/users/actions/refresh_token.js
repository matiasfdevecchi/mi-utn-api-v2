const { refreshToken } = require("../services/user_service")

module.exports.RefreshToken = async (req) => {
    return await refreshToken(req);
}