const User = require("../infrastructure/user")

module.exports = async id => {
    return await User.find(id);
}