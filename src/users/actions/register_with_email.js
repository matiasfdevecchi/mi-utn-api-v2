const { registerWithEmail } = require("../services/user_service")

module.exports.RegisterWithEmail = async (req, role) => {
    const {email, password} = req.body;
    return await registerWithEmail(email, password, role);
}