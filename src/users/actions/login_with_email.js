const { loginWithEmail } = require("../services/user_service");

module.exports.LoginWithEmail = async (req) => {
    const {email, password} = req.body;
    return await loginWithEmail(email, password);
}