const express = require('express');
const validateInput = require('../middlewares/validateInput');
const validateIsAdminRole = require('../middlewares/validateIsAdminRole');
const validateToken = require('../middlewares/validateToken');
const CreateModality = require('./actions/create_modality');
const { CreateModalityValidator } = require('./validators');
const router = express.Router()

router.post('/', [validateToken, validateIsAdminRole, CreateModalityValidator, validateInput], async (req, res) => {
    const { status, json } = await CreateModality(req.body.value);
    return res.status(status).json(json);
})

module.exports = router;