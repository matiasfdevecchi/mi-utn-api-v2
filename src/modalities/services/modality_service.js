const Modality = require("../infrastructure/modality")

module.exports.createModality = async value => {
    return await Modality.create(value);
}

module.exports.getAllModalities = async () => {
    return await Modality.getAll();
}

module.exports.updateModality = async (id, value) => {
    return await Modality.updateById(id, value);
}

module.exports.deleteModality = async id => {
    return await Modality.deleteById(id);
}