const { updateModality } = require("../services/modality_service")

module.exports = async (id, value) => {
    return await updateModality(id, value);
}