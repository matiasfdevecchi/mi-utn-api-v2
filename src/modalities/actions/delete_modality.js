const { deleteModality } = require("../services/modality_service")

module.exports = async id => {
    return await deleteModality(id);
}