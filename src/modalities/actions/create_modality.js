const { createModality } = require("../services/modality_service")

module.exports = async value => {
    return await createModality(value);
}