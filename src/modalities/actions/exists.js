const Modality = require("../infrastructure/modality")

module.exports = async id => {
    return await Modality.exists(id);
}