const { getAllModalities } = require("../services/modality_service")

module.exports = async () => {
    return await getAllModalities();
}