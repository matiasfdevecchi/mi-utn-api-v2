const { check } = require("express-validator");

module.exports.CreateModalityValidator = [
    check('value')
        .not()
        .isEmpty()
        .withMessage('El valor es requerido')
        .isLength({ min: 1, max: 30 })
        .withMessage('Valor debe ser de entre 1 y 30 caracteres'),
]