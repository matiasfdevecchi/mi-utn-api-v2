var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("Conected Modality!");
});

var modality_schema = new Schema({
    value: {
        type: String,
        required: [true, "Valor es requerido."],
    },
    created_at: {
        type: Date,
    },
    updated_at: {
        type: Date,
    },
});

modality_schema.pre("save", function (next) {
    const now = new Date;
    this.updated_at = now;
    if (this.isNew) {
        this.created_at = now;
    }
    next();
});

modality_schema.statics.create = async (value) => {
    try {
        const modality = new Modality({
            value
        })
        await modality.save();
        const json = {
            ok: true,
            msg: 'Modalidad creada con éxito.',
            id: modality._id,
        }
        return {
            status: 201,
            json,
        }
    } catch (e) {
        const json = {
            ok: false,
            msg: 'No se ha podido crear la modalidad. Inténtelo nuevamente.',
        }
        return {
            status: 500,
            json,
        }
    }
}

modality_schema.statics.getAllModalities = async () => {
    try {
        const modalities = await Modality.find();
        return {
            status: 200,
            json: {
                modalities
            }
        }
    } catch (e) {
        return {
            status: 500,
            json: {
                msg: 'No se pudo obtener las modalidades.'
            }
        }
    }
}

modality_schema.statics.exists = async id => {
    return await Modality.findById(id) !== null;
}

modality_schema.statics.updateById = async (id, value) => {
    try {
        const mod = await Modality.findById(id);
        if (!mod) {
            return {
                status: 404,
                json: {
                    msg: 'Modalidad no encontrada.',
                },
            };
        }
        mod.value = value;
        await mod.save();
        return {
            status: 200,
            json: {
                msg: 'Modalidad actualizada.',
            }
        }
    } catch (e) {
        return {
            status: 500,
            json: {
                msg: 'No se ha podido actualizar la modalidad. Inténtelo nuevamente.',
            }
        }
    }
}

modality_schema.statics.deleteById = async (id) => {
    try {
        const mod = await Modality.findByIdAndDelete(id);
        if (!mod) {
            return {
                status: 404,
                json: {
                    msg: 'Modalidad no encontrada.',
                },
            };
        }
        return {
            status: 200,
            json: {
                msg: 'Modalidad eliminada.',
            }
        }
    } catch (e) {
        return {
            status: 500,
            json: {
                msg: 'No se ha podido eliminar la modalidad. Inténtelo nuevamente.',
            }
        }
    }
}

var Modality = mongoose.model("Modality", modality_schema);

module.exports = Modality;