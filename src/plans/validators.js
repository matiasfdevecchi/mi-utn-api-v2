const { check } = require("express-validator");
const ExistsUniversity = require("../universities/actions/exists");
const ExistsSubject = require("../subjects/actions/exists");
const ObjectId = require('mongoose').Types.ObjectId;
const ExistsModality = require("../modalities/actions/exists");

module.exports.CreateValidator = [
    check('name')
        .not()
        .isEmpty()
        .withMessage('El nombre es requerido.')
        .isLength({ min: 5, max: 50 })
        .withMessage('Valor debe ser de entre 5 y 50 caracteres.'),
    check('university')
        .not()
        .isEmpty()
        .withMessage('La universidad es requerida.')
        .isMongoId()
        .withMessage('Universidad invalida.')
        .custom(async value => {
            const exists = await ExistsUniversity(value);
            if (!exists)
                throw new Error('La universidad seleccionada no existe.');
        }),
    check('subjects')
        .isArray()
        .withMessage('Asignaturas invalidas.')
        .custom(async subjects => {
            subjects.forEach(async subject => {
                await validateSubject(subject, 'asignaturas');
            })
        }),
    check('electives')
        .isArray()
        .withMessage('Electivas invalidas.')
        .custom(async electives => {
            electives.forEach(async elective => {
                await validateSubject(elective, 'electivas');
            })
        }),
    check('electivesRules')
        .isArray()
        .withMessage('Reglas invalidas.')
        .custom(async electivesRules => {
            electivesRules.forEach(rule => {
                const { level, hours } = rule;
                if (!Number.isInteger(level) || !Number.isInteger(hours))
                    throw new Error('Una de las reglas es invalida.');
            })
        }),
];

const validateSubject = async (subject, topic) => {
    const { subject: subjectId,
        level,
        modality,
        subjects_cursadas_para_cursar,
        subjects_aprobadas_para_cursar,
        subjects_cursadas_para_rendir,
        subjects_aprobadas_para_rendir } = subject;

    if (!ObjectId.isValid(subjectId) || !Number.isInteger(level) || !ObjectId.isValid(modality))
        throw new Error(`Una de las ${topic} es invalida.`);

    await ExistsModality(modality);

    await validateSubjectIds([subjectId], topic);
    await validateSubjectIds(subjects_cursadas_para_cursar, topic);
    await validateSubjectIds(subjects_aprobadas_para_cursar, topic);
    await validateSubjectIds(subjects_cursadas_para_rendir, topic);
    await validateSubjectIds(subjects_aprobadas_para_rendir, topic);
}

const validateSubjectIds = async (subjects, topic) => {
    subjects.forEach(async subject => {
        const exists = await ExistsSubject(subject)
        if (!exists)
            throw new Error(`Una de las ${topic} es invalida.`);
    })
}