const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("Conected Plan!");
});

const subject_schema = {
    subject: {
        type: Schema.Types.ObjectId,
        ref: 'Subject',
        required: [true, 'Asignatura es requerida.'],
    },
    level: {
        type: Number,
        required: [true, 'Nivel de la asignatura es requerido.'],
    },
    modality: {
        type: Schema.Types.ObjectId,
        ref: 'Modality',
        required: [true, 'Modalidad es requerida.'],
    },
    subjects_cursadas_para_cursar: {
        type: Schema.Types.ObjectId,
        ref: 'Subject',
        required: [true, 'Asignatura es requerida.'],
    },
    subjects_aprobadas_para_cursar: {
        type: Schema.Types.ObjectId,
        ref: 'Subject',
        required: [true, 'Asignatura es requerida.'],
    },
    subjects_cursadas_para_rendir: {
        type: Schema.Types.ObjectId,
        ref: 'Subject',
        required: [true, 'Asignatura es requerida.'],
    },
    subjects_aprobadas_para_rendir: {
        type: Schema.Types.ObjectId,
        ref: 'Subject',
        required: [true, 'Asignatura es requerida.'],
    },
};

const electives_rule_schema = {
    level: {
        type: Number,
        required: [true, 'El nivel es requerido.'],
    },
    hours: {
        type: Number,
        required: [true, 'Las horas son requeridas.'],
    },
}

const plan_schema = new Schema({
    name: {
        type: String,
        required: [true, "Nombre es requerido."],
    },
    university: {
        type: Schema.Types.ObjectId,
        ref: 'University',
        required: [true, 'Universidad es requerida.'],
    },
    subjects: [subject_schema],
    electives: [subject_schema],
    electivesRules: [electives_rule_schema],
    published: {
        type: Boolean,
        default: false,
    },
    creator: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: [true, "El creador es requerido"],
    },
    official: {
        type: Boolean,
        default: false,
    },
    created_at: {
        type: Date,
    },
    updated_at: {
        type: Date,
    },
});

plan_schema.pre("save", function (next) {
    const now = new Date;
    this.updated_at = now;
    if (this.isNew) {
        this.created_at = now;
    }
    next();
});

plan_schema.statics.create = async (data) => {
    try {
        const plan = new Plan({
            ...data
        })
        await plan.save();
        const json = {
            ok: true,
            msg: 'Rol creado con éxito.',
            id: plan._id,
        }
        return {
            status: 201,
            json,
        }
    } catch (e) {
        const json = {
            ok: false,
            msg: 'No se ha podido crear el plan. Inténtelo nuevamente.',
            error: e,
        }
        return {
            status: 500,
            json,
        }
    }
}

plan_schema.statics.updateById = async (userId, planId, data) => {
    try {
        let plan = await Plan.findById(planId);
        if (!plan) {
            return {
                status: 404,
                json: {
                    msg: 'Plan no encontrado.',
                },
            };
        }

        if (String(plan.creator) !== userId) {
            return {
                status: 404,
                json: {
                    msg: 'No sos el creador del plan. No tenés permisos para modificarlo.',
                },
            };
        }

        Object.assign(plan, data);
        
        await plan.save();
        return {
            status: 200,
            json: {
                msg: 'Plan actualizado.',
            }
        }
    } catch (e) {
        return {
            status: 500,
            json: {
                msg: 'No se ha podido actualizar el plan. Inténtelo nuevamente.',
                error: e,
            }
        }
    }
}

var Plan = mongoose.model("Plan", plan_schema);

module.exports = Plan;