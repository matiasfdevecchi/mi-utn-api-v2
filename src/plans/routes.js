const express = require('express');
const validateInput = require('../middlewares/validateInput');
const validateToken = require('../middlewares/validateToken');
const CreatePlan = require('./actions/create_plan');
const UpdatePlan = require('./actions/update_plan');
const { CreateValidator } = require('./validators');
const router = express.Router()

router.post('/', [validateToken, CreateValidator, validateInput], async (req, res) => {
    const { status, json } = await CreatePlan({
        ...req.body,
        creator: req.uid,
    });
    return res.status(status).json(json);
})

router.put('/:planId', [validateToken, CreateValidator, validateInput], async (req, res) => {
    const { status, json } = await UpdatePlan(req.uid, req.params.planId, req.body);
    return res.status(status).json(json);
})

module.exports = router;