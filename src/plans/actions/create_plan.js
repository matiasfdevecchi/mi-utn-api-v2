const Plan = require("../infrastructure/plan")

module.exports = async plan => {
    return await Plan.create(plan);
}