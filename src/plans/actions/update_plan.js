const Plan = require("../infrastructure/plan")

module.exports = async (userId, planId, data) => {
    return await Plan.updateById(userId, planId, data);
}