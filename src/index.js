const express = require('express');
require('dotenv').config();
const cors = require('cors');
const helmet = require('helmet');

const { dbConnection } = require('./database/config');

const departmentsRouter = require('./departments/routes');
const modalitiesRouter = require('./modalities/routes');
const plansRouter = require('./plans/routes');
const subjectsRouter = require('./subjects/routes');
const universitiesRouter = require('./universities/routes');
const usersRouter = require('./users/routes/users_routes');

dbConnection();

const PORT = process.env.PORT ?? 8000

console.log(PORT);

const app = express();

app.use(helmet());
app.use(cors());
app.use(express.json());

app.get('/', (req, res) => res.send('Bienvenido a Mi UTN 2'));

app.use('/api/departments', departmentsRouter)
app.use('/api/modalities', modalitiesRouter)
app.use('/api/plans', plansRouter)
app.use('/api/subjects', subjectsRouter)
app.use('/api/universities', universitiesRouter)
app.use('/api/users', usersRouter)

app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
});