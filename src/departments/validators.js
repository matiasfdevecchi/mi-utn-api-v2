const { check } = require("express-validator");

module.exports.CreateValidator = [
    check('name')
        .not()
        .isEmpty()
        .withMessage('El nombre es requerido')
        .isLength({ min: 1, max: 100 })
        .withMessage('Nombre debe ser de entre 1 y 100 caracteres'),
]