const Department = require("../infrastructure/department")

module.exports = async name => {
    return await Department.create(name);
}