const Department = require("../infrastructure/department")

module.exports = async id => {
    return await Department.exists(id);
}