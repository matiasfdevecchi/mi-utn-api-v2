var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("Conected Department!");
});

var department_schema = new Schema({
    name: {
        type: String,
        required: [true, "Nombre es requerido."],
    },
    created_at: {
        type: Date,
    },
    updated_at: {
        type: Date,
    },
});

department_schema.pre("save", function (next) {
    const now = new Date;
    this.updated_at = now;
    if (this.isNew) {
        this.created_at = now;
    }
    next();
});

department_schema.statics.create = async name => {
    try {
        const department = new Department({
            name
        })
        await department.save();
        const json = {
            ok: true,
            msg: 'Departamento creado con éxito.',
            id: department._id,
        }
        return {
            status: 201,
            json,
        }
    } catch (e) {
        const json = {
            ok: false,
            msg: 'No se ha podido crear el departamento. Inténtelo nuevamente.',
            error: e,
        }
        return {
            status: 500,
            json,
        }
    }
} 

department_schema.statics.exists = async id => {
    return await Department.findById(id) !== null;
}

var Department = mongoose.model("Department", department_schema);

module.exports = Department;