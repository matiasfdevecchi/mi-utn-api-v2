const express = require('express');
const validateInput = require('../middlewares/validateInput');
const validateIsAdminRole = require('../middlewares/validateIsAdminRole');
const validateToken = require('../middlewares/validateToken');
const CreateDepartment = require('./actions/create');
const { CreateValidator } = require('./validators');
const router = express.Router()

router.post('/', [validateToken, validateIsAdminRole, CreateValidator, validateInput], async (req, res) => {
    const { status, json } = await CreateDepartment(req.body.name);
    return res.status(status).json(json);
})

module.exports = router;