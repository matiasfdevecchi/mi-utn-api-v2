const Subject = require("../infrastructure/subject")

module.exports = async id => {
    return await Subject.exists(id);
}