const Subject = require("../infrastructure/subject")

module.exports = async (name, department, creator) => {
    return await Subject.create(name, department, creator);
}