const Subject = require("../infrastructure/subject")

module.exports = async query => {
    return await Subject.getByQuery(query);
}