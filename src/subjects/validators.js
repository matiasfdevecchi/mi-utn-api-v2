const { check } = require("express-validator");
const ExistsDepartment = require("../departments/actions/exists");

module.exports.CreateValidator = [
    check('name')
        .not()
        .isEmpty()
        .withMessage('El nombre es requerido')
        .isLength({ min: 1, max: 100 })
        .withMessage('Nombre debe ser de entre 1 y 100 caracteres'),
    check('department')
        .not()
        .isEmpty()
        .withMessage('El departamento es requerido.')
        .isMongoId()
        .withMessage('El departamento es invalido.')
        .custom(async value => {
            const exists = await ExistsDepartment(value);
            if (!exists)
                throw new Error('El departamento seleccionado no existe.');
        }),
]