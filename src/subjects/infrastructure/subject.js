var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("Conected Subject!");
});

var subject_schema = new Schema({
    name: {
        type: String,
        required: [true, "Nombre es requerido."],
    },
    department: {
        type: Schema.Types.ObjectId,
        ref: 'Department',
        required: [true, 'Departamento es requerido.'],
    },
    creator: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: [true, "El creador es requerido"],
    },
    official: {
        type: Boolean,
        default: false,
    },
    created_at: {
        type: Date,
    },
    updated_at: {
        type: Date,
    },
});

subject_schema.pre("save", function (next) {
    const now = new Date;
    this.updated_at = now;
    if (this.isNew) {
        this.created_at = now;
    }
    next();
});

subject_schema.statics.create = async (name, department, creator) => {
    try {
        const subject = new Subject({
            name: name.toLowerCase(),
            department,
            creator,
        })
        await subject.save();
        const json = {
            ok: true,
            msg: 'Asignatura creada con éxito.',
            id: subject._id,
        }
        return {
            status: 201,
            json,
        }
    } catch (e) {
        const json = {
            ok: false,
            msg: 'No se ha podido crear la asignatura. Inténtelo nuevamente.',
        }
        return {
            status: 500,
            json,
        }
    }
}

subject_schema.statics.exists = async id => {
    return await Subject.findById(id) !== null;
}

subject_schema.statics.getByQuery = async query => {
    try {
        let regex = '.*';
        if (query != null || query.length > 0)
            regex = query
                .toLowerCase()
                .replace(/[aá]/g, '[aá]')
                .replace(/[eé]/g, '[eé]')
                .replace(/[ií]/g, '[ií]')
                .replace(/[oó]/g, '[oó]')
                .replace(/[uú]/g, '[uú]')
        console.log(regex);
        const subjects = await Subject.find({ name: { '$regex': regex, '$options': 'i' } });
        return {
            status: 200,
            json: {
                subjects,
            },
        };
    } catch (e) {
        console.log(e);
        const json = {
            msg: 'No se ha podido realizar la búsqueda. Inténtelo nuevamente.',
        }
        return {
            status: 500,
            json,
        }
    }
}

var Subject = mongoose.model("Subject", subject_schema);

module.exports = Subject;