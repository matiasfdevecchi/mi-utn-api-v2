const express = require('express');
const validateInput = require('../middlewares/validateInput');
const validateToken = require('../middlewares/validateToken');
const CreateSubject = require('./actions/create');
const getByQuery = require('./actions/getByQuery');
const { CreateValidator } = require('./validators');
const router = express.Router()

router.post('/', [validateToken, CreateValidator, validateInput], async (req, res) => {
    const { status, json } = await CreateSubject(req.body.name, req.body.department, req.uid);
    return res.status(status).json(json);
})

router.get('/', validateToken, async (req, res) => {
    const { status, json } = await getByQuery(req.query.q);
    return res.status(status).json(json);
})

module.exports = router;