var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("Conected Role!");
});

var role_schema = new Schema({
    value: {
        type: String,
        required: [true, "Valor es requerido."],
    },
    created_at: {
        type: Date,
    },
    updated_at: {
        type: Date,
    },
});

role_schema.pre("save", function (next) {
    const now = new Date;
    this.updated_at = now;
    if (this.isNew) {
        this.created_at = now;
    }
    next();
});

role_schema.statics.create = async (value) => {
    try {
        const role = new Role({
            value
        })
        await role.save();
        const json = {
            ok: true,
            msg: 'Rol creado con éxito.',
            id: role._id,
        }
        return {
            status: 201,
            json,
        }
    } catch (e) {
        const json = {
            ok: false,
            msg: 'No se ha podido crear el rol. Inténtelo nuevamente.',
        }
        return {
            status: 500,
            json,
        }
    }
} 

role_schema.statics.findByValue = async value => {
    return await Role.findOne({value});
}

var Role = mongoose.model("Role", role_schema);

module.exports = Role;