const Role = require("../infrastructure/Role")

module.exports.getOrCreateRole = async (value) => {
    const role = await Role.findByValue(value);
    if (!role) {
        return await Role.create(value);
    }
    return {
        status: 200,
        json: {
            ok: true,
            msg: 'Rol obtenido con éxito.',
            id: role._id,
        }
    }
}