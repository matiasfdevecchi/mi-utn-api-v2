const { getOrCreateRole } = require("../services/role_service");
const values = require("../values");

module.exports = async () => {
    return getOrCreateRole(values.DEFAULT);
}