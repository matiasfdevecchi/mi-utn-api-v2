const mongoose = require('mongoose');

module.exports.dbConnection = async() => {

    try {
        await mongoose.connect( process.env.MONGO_URL , {
            useNewUrlParser: true, 
            useUnifiedTopology: true,
            useCreateIndex: true
        });

        console.log('MongoDB connected.');


    } catch (error) {
        console.log(error);
        throw new Error('Error initializing MongoDB');
    }
}