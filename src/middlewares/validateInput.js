const { response } = require('express');
const { validationResult } = require('express-validator');

module.exports = (req, res = response, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({
            ok: false,
            msg: errors.array()[0]['msg'],
            error: errors,
        });
    }
    next();
}