const { response } = require('express');
const GetAdminRole = require('../roles/actions/get_admin_role');
const GetUserById = require('../users/actions/get_user_by_id');

module.exports = async (req, res = response, next) => {
    try {
        const { uid } = req;
        const { role:userRoleId } = await GetUserById(uid);
        const { json } = await GetAdminRole();
        const { id:adminRoleId } = json;

        if (String(userRoleId) === String(adminRoleId)) 
            next();
        else
            return res.status(401).json({
                msg: 'Permiso denegado.',
            });
    } catch (e) {
        console.log(e);
        return res.status(500).json({
            msg: 'Error chequeando el rol del usuario.',
        });
    }
}