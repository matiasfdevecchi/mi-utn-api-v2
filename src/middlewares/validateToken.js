const { response } = require('express');
const jwt = require('jsonwebtoken');

module.exports = (req, res = response, next) => {

    const token = req.header('Authorization');
    if (!token) {
        return res.status(401).json({
            ok: false,
            msg: 'No hay token en la petición.'
        });
    }

    try {
        const simplifiedToken = token.split('Bearer ')[1];
        const { uid } = jwt.verify(
            simplifiedToken,
            process.env.SECRET_JWT_SEED
        );
        req.uid = uid;
    } catch (error) {
        return res.status(401).json({
            ok: false,
            msg: 'Token no válido.'
        });
    }
    next();
}